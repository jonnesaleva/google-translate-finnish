---
title: Finnish to English and Back with Google Translate
author: Jonne Saleva
date: 02/06/2020
---

# Introduction

- Finnish: a language with only ca. 6 million speakers
- Both "low-resource" and "morphologically rich", so very challenging for NLP
- According to some, Finnish is the adversarial "adversarial example" to test your NLP models

# Finnish complexity: one word can say a lot
- juosta = to run
- juoksennella = to run around/continuously
- juoksentelen = I run around aimlessly.
- juoksentelenko = Do I / Will I run around aimlessly?
- juoksentelenkohan = I wonder if I do/will run around aimlessly?

# Google Translate
- Until very recently, Google Translate was really bad at translating to/from Finnish
- I wanted to see how well GT does with Finnish and English, going in either direction
- If it did well, I wanted to see if there are certain types of texts that it struggles with, e.g. idiomatic expressions

# Level 1: Easy Finnish
- Short news reports in "Easy/Simplified Finnish"
- Intended mostly for those whose native language is not Finnish, e.g. recent immigrants

# Eduskunta aloitti työt

Eduskunta on palannut joululomalta takaisin töihin.

Kevään ensimmäisessä täysistunnossa eduskunnan puhemieheksi valittiin keskustapuolueen Matti Vanhanen. Varapuhemiehiksi valittiin SDP:n Antti Rinne ja perussuomalaisten Juho Eerola.

Kevätistuntokauden varsinainen avaus eli valtiopäivien avajaiset ovat huomenna. Valtiopäivät avaa presidentti Sauli Niinistö.

# Parliament started work

Eduskunta on palannut joululomalta takaisin töihin.

Kevään ensimmäisessä täysistunnossa eduskunnan puhemieheksi valittiin keskustapuolueen Matti Vanhanen. Varapuhemiehiksi valittiin SDP:n Antti Rinne ja perussuomalaisten Juho Eerola.

Kevätistuntokauden varsinainen avaus eli valtiopäivien avajaiset ovat huomenna. Valtiopäivät avaa presidentti Sauli Niinistö.

# Parliament started work

Parliament has returned from work on Christmas holidays to work.

Kevään ensimmäisessä täysistunnossa eduskunnan puhemieheksi valittiin keskustapuolueen Matti Vanhanen. Varapuhemiehiksi valittiin SDP:n Antti Rinne ja perussuomalaisten Juho Eerola.

Kevätistuntokauden varsinainen avaus eli valtiopäivien avajaiset ovat huomenna. Valtiopäivät avaa presidentti Sauli Niinistö.

# Parliament started work

Parliament has returned from work on Christmas holidays to work.

At the first plenary session of the spring, Matti Vanhanen was elected as the Speaker of the Parliament. Antti Rinne of the SDP and Juho Eerola of the Basic Finns were elected Vice-Presidents.

Kevätistuntokauden varsinainen avaus eli valtiopäivien avajaiset ovat huomenna. Valtiopäivät avaa presidentti Sauli Niinistö.

# Parliament started work

Parliament has returned from work on Christmas holidays to work.

At the first plenary session of the spring, Matti Vanhanen was elected as the Speaker of the Parliament. Antti Rinne of the SDP and Juho Eerola of the Basic Finns were elected Vice-Presidents.

The actual opening of the spring session, the opening of the Parliament, is tomorrow. President Sauli Niinistö opens the Diet.

# Parlamentti aloitti työn

Parlamentti on palannut joululomien töistä töihin.

Kevään ensimmäisessä täysistunnossa Matti Vanhanen valittiin parlamentin puhemieheksi. Varapuheenjohtajiksi valittiin SDP: n Antti Rinne ja perussuomalaisten Juho Eerola.

Kevään istunnon varsinainen avaaminen, parlamentin avaaminen, on huomenna. Presidentti Sauli Niinistö avaa ruokavalion.

# Analysis
- Overall I would say the translation performance is reasonable
- Error type 1: Extraneous insertion of tokens
    - Parliament has returned from work on Christmas holidays to work.
- Error type 2: Lexical errors
    - GT correctly translates "puhemies" to "Speaker of the Parliament"
    - However "varapuhemies" = "vara" + "puhemies" is translated as "Vice-President", which is incorrect
- Error type 3: Lexical ambiguity
    - "Diet" gets translated to "ruokavalio" when going EN-FI
    - Technically correct but not in context
- Error type 4: propagation of errors
    - Anything the FI -> EN gets wrong, EN -> FI will most likely get wrong while backtranslating

# Level 2: Actual news by YLE (Finnish Broadcasting Corp.)
- Article about polling figures from this week
- Contains more idiomatic expressions than "Easy Finnish"
- Arguably, should be harder to translate

# Link to the entire text and translation

- https://www.gitlab.com/jonnesaleva/google-translate-finnish
- https://yle.fi/uutiset/3-11195795

# Error analysis

- Even FI-EN is bad with more idiomatic newswire & multi-meaning words
    - FI: Keskustan Kulmuni niukkasanainen puolueen heikoista kannatusluvuista
    - TR: "Kulmuni of the Center Party terse/tight-lipped about the party's new poll numbers"
    - EN: I'm centering Kulmuni's scant word on the party's weak support
    - BT: Keskityn Kulmunin niukkaan sanan puolueen heikkoihin tukihahmoihin
        - This makes absolutely no sense and has several case errors
    - "Keskusta" -> "I'm centering" -> "Keskityn"

# Error analysis

- Idioms & ambiguous words are problematic
    - FI: Keskustan uudet kannatusluvut vetivät puolueen puheenjohtajan, valtiovarainministeri Katri Kulmunin lyhytsanaiseksi torstaina eduskunnassa.
    - TR: The new Center Party polling numbers made the party leader, Finance Minister Katri Kulmuni tight-lipped in Parliament on Thursday.
    - EN: New party support figures in the center drew Katri Kulmun, the party's chairman, Finance Minister, on Thursday in Parliament.
    - BT: Keskustan uudet puoluetukihahmot vetivät torstaina eduskunnassa puolueen puheenjohtajan, talousministeri Katri Kulmunin.
        - "hahmo" = "figure" (i.e. character), but "luku" = "figure" (i.e. number)

# Error analysis

- Named entities are translated badly
    - For instance, "Kulmuni" is the last name of the current Finance Minister
    - FI: Kulmuni toisti, että puolue tekee tai sen täytyy tehdä nyt kovasti töitä.
    - TR: Kulmuni reiterated that the party is or has to work hard
    - EN: My eyebrow reiterated that the party is or must work hard now.
    - BT: Kulmakarvani toisti, että puolue on tai sen täytyy työskennellä kovasti nyt.

# Error analysis 

- Named entities are translated badly
    - "Keskusta" means all of "downtown", "the Center Party", "the center"
    - FI: Ylen tammikuun mittauksessa keskustan kannatus laski 10,8 prosenttiin.
    - TR: In Yle's January polls, the Center Party approval fell to 10.8 percent.
    - EN: In the January survey, downtown support fell to 10.8 percent

# Round 3: Self-created adversarial examples
- juosta = to run = run
- juoksen = I run = I run
- juoksenko = do I run? = I run
- juoksenkohan = I wonder if I run? = I run
- juoksennella = run around = run around
- juoksentelen = I run around = running around
- juoksentelenko = Do I run around? = running arond
- juoksentelenkohan = I wonder if I will run aroudn? = juoksentelenkohan

# Final comments
- Overall the translation performance is remarkably good, across texts. Much better than expected.
- Simple texts are translated very well, and across all texts things like case agreement are generally handled correclty
    - In fact only 1 instance of incorrect case in a correct translation
- Ambiguity is very problematic, but GT seems to not be taking into account lots of context
- Back-translation is (obviously) hard, and the quality of the output is quite questionable
- Self-created adversarial examples that are relatively easy for a native speaker are currently still impossible for GT

# Final verdict

- Good job Google Translate, you get a B+
