# Kuntien irtisanomiset

Kunnat joutuvat tänä vuonna irtisanomaan ja lomauttamaan työntekijöitä enemmän kuin tavallisesti.

Kuntien järjestö Kuntatyönantajat sanoo, että kunnat ja kuntayhtymät joutuvat tänä vuonna irtisanomaan ehkä yli 750 työntekijää. Lisäksi ehkä yli 8000 ihmistä lomautetaan.

Kuntatyöantajat sanoo, että tätä suurempia irtisanomisia kunnissa tehtiin viimeksi 1990-luvun lamavuosina.

Irtisanomiset ja lomautukset johtuvat kuntien huonosta taloustilanteesta.

Kuntatyönantajien arvio perustuu kyselyyn, jonka se teki kunnille joulukuussa.

# Eduskunta aloitti työt

Eduskunta on palannut joululomalta takaisin töihin.

Kevään ensimmäisessä täysistunnossa eduskunnan puhemieheksi valittiin keskustapuolueen Matti Vanhanen. Varapuhemiehiksi valittiin SDP:n Antti Rinne ja perussuomalaisten Juho Eerola.

Kevätistuntokauden varsinainen avaus eli valtiopäivien avajaiset ovat huomenna. Valtiopäivät avaa presidentti Sauli Niinistö.

