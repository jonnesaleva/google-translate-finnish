# I'm centering Kulmuni's scant word on the party's weak support figures: "Last month up, now down"

New party support figures in the center drew Katri Kulmun, the party's chairman, Finance Minister, on Thursday in Parliament.

My eyebrow reiterated that the party is or must work hard now.

In the January survey, downtown support fell to 10.8 percent. The figure is close to the party's all-time low last November, with 10.6 percent of those polling the center.

In December's measurement, downtown support rose by 1.4 percentage points, which Kulmuni said at the time as hope-creating news.

## Why is downtown support declining again?

- Last month we went up and this month we came down again. The city center works hard with humble minds. It is essential for us that the motherland will do well, Kulmuni replied before the party group meeting.

## Downtown support fell close to the lowest readings it had in the YLE poll, why is it?

- The center must work hard to gain the trust of Finns, and we do it every day.

## What kind of alarm bells are ringing in the party now?

- We work for the Finns wherever they live in this country. One month goes a little up, another month a step down.

## Are you worried that work doesn't seem to be paying off?

- Last month we went up, now we came down. Let's see how next month. We work long-term.

## Is the trend down yet?

- We work for the Finns. That's what the center is for.

