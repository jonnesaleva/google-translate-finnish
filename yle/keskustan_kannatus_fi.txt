# Keskustan Kulmuni niukkasanainen puolueen heikoista kannatusluvuista: "Viime kuussa ylös, nyt alas"

Keskustan uudet kannatusluvut vetivät puolueen puheenjohtajan, valtiovarainministeri Katri Kulmunin lyhytsanaiseksi torstaina eduskunnassa.

Kulmuni toisti, että puolue tekee tai sen täytyy tehdä nyt kovasti töitä.

Ylen tammikuun mittauksessa keskustan kannatus laski 10,8 prosenttiin. Lukema on lähellä puolueen kaikkien aikojen heikointa mittaustulosta viime marraskuussa, jolloin keskustan kannattajiksi ilmoittautui 10,6 prosenttia vastanneista.

Joulukuun mittauksessa keskustan kannatus nousi 1,4 prosenttiyksiköllä, mitä Kulmuni sanoi tuolloin toivoa luovaksi uutiseksi.

## Miksi keskustan kannatus laskee jälleen alas?

– Viime kuussa mentiin ylös ja tässä kuussa tultiin taas vähän alas. Keskusta tekee nöyrin mielin kovasti työtä. Se on meille keskeistä, että isänmaa pärjää, Kulmuni vastasi ennen puolueen ryhmäkokousta.

## Keskustan kannatus laski lähelle alhaisimpia lukemia mitä sille on mitattu Ylen kyselyssä, mistä se johtuu?

– Keskustan täytyy tehdä kovasti työtä suomalaisten luottamuksen saamiseksi, ja sitä me teemme joka päivä.

## Millaiset hälytyskellot puolueessa nyt soivat?

– Me teemme töitä suomalaisten puolesta missä tahansa he asuvat tässä maassa. Joku kuukausi mennään hieman ylöspäin, toisena kuukautensa tullaan askel alaspäin.

## Huolestuttaako se, että työ ei näköjään kanna tulosta?

– Viime kuussa mentiin ylös, nyt tultiin alas. Katsotaan miten ensi kuussa. Teemme pitkäjänteisesti työtä.

## Trendi on kuitenkin aika alhaalla?

– Teemme suomalaisten puolesta työtä. Se on se juttu mitä varten keskusta on olemassa.
